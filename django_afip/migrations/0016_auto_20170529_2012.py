# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-05-29 20:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('afip', '0015_receiptentry__amount_to_quantity'),
    ]

    operations = [
        migrations.AlterField(
            model_name='receipt',
            name='issued_date',
            field=models.DateField(help_text='Can diverge up to 5 days for goods, or 10 days otherwise', verbose_name='issued date'),
        ),
        migrations.AlterField(
            model_name='receiptentry',
            name='quantity',
            field=models.PositiveSmallIntegerField(verbose_name='quantity'),
        ),
        migrations.AlterField(
            model_name='receiptpdf',
            name='sales_terms',
            field=models.CharField(help_text='Should be something like "Cash", "Payable in 30 days", etc', max_length=48, verbose_name='sales terms'),
        ),
        migrations.AlterField(
            model_name='receiptvalidation',
            name='cae',
            field=models.CharField(help_text='The CAE as returned by the AFIP', max_length=14, verbose_name='cae'),
        ),
        migrations.AlterField(
            model_name='receiptvalidation',
            name='cae_expiration',
            field=models.DateField(help_text='The CAE expiration as returned by the AFIP', verbose_name='cae expiration'),
        ),
        migrations.AlterField(
            model_name='receiptvalidation',
            name='observations',
            field=models.ManyToManyField(help_text='The observations as returned by the AFIP. These are generally present for failed validations.', related_name='validations', to='afip.Observation', verbose_name='observations'),
        ),
        migrations.AlterField(
            model_name='receiptvalidation',
            name='receipt',
            field=models.OneToOneField(help_text='The Receipt for which this validation applies', on_delete=django.db.models.deletion.CASCADE, related_name='validation', to='afip.Receipt', verbose_name='receipt'),
        ),
        migrations.AlterField(
            model_name='receiptvalidation',
            name='result',
            field=models.CharField(choices=[('A', 'approved'), ('R', 'rejected')], help_text='Indicates whether the validation was succesful or not', max_length=1, verbose_name='result'),
        ),
        migrations.AlterField(
            model_name='receiptvalidation',
            name='validation',
            field=models.ForeignKey(help_text='The validation for the batch that produced this instance.', on_delete=django.db.models.deletion.CASCADE, related_name='receipts', to='afip.Validation', verbose_name='validation'),
        ),
    ]
